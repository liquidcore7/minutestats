package com.minutestats.store

case class RequestData(amount: Double, timestamp: Long)

class StatsUnit(val sum: Double, val min: Double, val max: Double, val count: Long, val identifiedBy: RequestData) {
  def avg: Double = sum / count
}

object StatsUnit {
  def onDeleteCorrection(original: StatsUnit, deletedUnit: StatsUnit, amount: Double): StatsUnit = {
    new StatsUnit(
      sum = original.sum - deletedUnit.identifiedBy.amount,
      min = if (deletedUnit.min == original.min) amount else original.min,
      max = if (deletedUnit.max == original.max) amount else original.max,
      count = original.count - 1,
      identifiedBy = original.identifiedBy
    )
  }

  def fromPreviousUnit(original: StatsUnit, insertedUnit: RequestData): StatsUnit = {
    new StatsUnit(
      sum = original.sum + insertedUnit.amount,
      min = if (insertedUnit.amount < original.min) insertedUnit.amount else original.min,
      max = if (insertedUnit.amount > original.max) insertedUnit.amount else original.max,
      count = original.count + 1,
      identifiedBy = insertedUnit
    )
  }

  def fromRequestData(requestData: RequestData) = new StatsUnit(
                                                  requestData.amount,
                                                  requestData.amount,
                                                  requestData.amount,
                                                  1L,
                                                  requestData)
}