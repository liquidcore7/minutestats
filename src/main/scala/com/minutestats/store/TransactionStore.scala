package com.minutestats.store

import java.util.concurrent.{Semaphore, TimeUnit}

import com.typesafe.scalalogging.Logger

class TransactionStore {
  private val data: TransactionHistory = new TransactionHistory
  private def time: Long = System.currentTimeMillis()
  private val SECOND: Long = 1000L // 1s == 1000ms
  private val lock: Semaphore = new Semaphore(0)

  private val removeRoutine: Thread = new Thread(new Runnable {
    override def run(): Unit = data.peekLast match {
        case None => lock.acquire(); run()
        case Some(statsUnit) =>
          if (statsUnit.identifiedBy.timestamp + 60*SECOND < time)
            data.remove()
          else
            lock.tryAcquire(statsUnit.identifiedBy.timestamp + 60 * SECOND - time, TimeUnit.MILLISECONDS)
          run()
    }
  })

  def store(transaction: RequestData): Either[String, Unit] = {
    if (time - transaction.timestamp > 60*SECOND)
      Left("Outdated transaction")
    else {
      data.insert(transaction, Some(lock))
      Right(())
    }
  }

  def statistics: Option[StatsUnit] = data.getStats
  def terminate(): Unit = removeRoutine.interrupt()
}

object TransactionStore {
  def apply: TransactionStore = {
    val newStore = new TransactionStore()
    newStore.removeRoutine.start()
    newStore
  }
}


