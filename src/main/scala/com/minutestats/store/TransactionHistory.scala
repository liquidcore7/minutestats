package com.minutestats.store

import java.util.concurrent.Semaphore

import com.typesafe.scalalogging.Logger

class TransactionHistory {

  class ListNode(val data: StatsUnit, var next: Option[TransactionHistory#ListNode]) {

    def insert(newNode: StatsUnit): Unit = next match {
      case None => next = Some(new ListNode(newNode, None))
      case Some(node) => next = Some(new ListNode(newNode, Some(node)))
    }

    def getInsertionPos(timestamp: Long): TransactionHistory#ListNode = next match {
      case None => this
      case Some(node) => if (node.data.identifiedBy.timestamp > timestamp) this else node.getInsertionPos(timestamp)
    }

  }
  private val logger: Logger = Logger("TransactionHistory")
  private var transactionHistoryHead: Option[TransactionHistory#ListNode] = None
  private var statsHead: Option[StatsUnit] = None

  private def updateOnRemove(removedUnit: StatsUnit): Unit = statsHead match {
    case None => ()
    case Some(stats) => statsHead = Some(StatsUnit.onDeleteCorrection(
                                        stats, removedUnit, transactionHistoryHead.get.data.identifiedBy.amount)
                                        )
  }

  // statsHead has incorrect identifiedBy field, however, it isn`t used
  private def updateOnInsert(insertedUnit: StatsUnit): Unit = statsHead match {
    case None => statsHead = Some(insertedUnit)
    case Some(stats) => statsHead = Some(StatsUnit.fromPreviousUnit(stats, insertedUnit.identifiedBy))
  }

  def insert(requestData: RequestData, notifier: Option[Semaphore] = None): Unit = {
    if (transactionHistoryHead.isEmpty || transactionHistoryHead.get.data.identifiedBy.timestamp > requestData.timestamp) {
      transactionHistoryHead = Some(new ListNode(StatsUnit.fromRequestData(requestData),
                                    if (transactionHistoryHead.isDefined) transactionHistoryHead else None))
      updateOnInsert(transactionHistoryHead.get.data)
      logger info "new head inserted, updating and notifying semaphore"
      if (notifier.isDefined)
        notifier.get.release()
    } else {
      val insertPoint = transactionHistoryHead.get.getInsertionPos(requestData.timestamp)
      val nextValue = StatsUnit.fromPreviousUnit(insertPoint.data, requestData)
      insertPoint.insert(nextValue)
      logger info "inserted new value somewhere else"
      updateOnInsert(nextValue)
    }
  }

  def remove(): Unit = transactionHistoryHead match {
    case None => ()
    case Some(node) => if (node.next.isEmpty) {
                      logger info "cleaned statsHead"
                      transactionHistoryHead = None
                      statsHead = None
                      } else {
                      transactionHistoryHead = node.next
                      updateOnRemove(node.data)
                      logger info "removed last from non-empty sequence"
                      }
  }

  def getStats: Option[StatsUnit] = statsHead
  def peekLast: Option[StatsUnit] = transactionHistoryHead match {
    case None => None
    case Some(node) => Some(node.data)
  }
}