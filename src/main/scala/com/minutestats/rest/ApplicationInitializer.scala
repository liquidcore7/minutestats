package com.minutestats.rest

import com.github.takezoe.resty.Resty
import com.minutestats.store.TransactionStore
import javax.servlet.{ServletContextEvent, ServletContextListener}
import javax.servlet.annotation.WebListener

@WebListener
class ApplicationInitializer extends ServletContextListener {
  private final val controller: RestController = new RestController(TransactionStore.apply)
  override def contextInitialized(sce: ServletContextEvent): Unit = Resty.register(controller)
  override def contextDestroyed(sce: ServletContextEvent): Unit = controller.store.terminate()
}
