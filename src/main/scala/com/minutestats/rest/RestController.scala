package com.minutestats.rest

import com.github.takezoe.resty.{Action, ActionResult}
import com.minutestats.store.{RequestData, TransactionStore}

class RestController(private[rest] final val store: TransactionStore) {

  @Action(method="GET", path="/statistics")
  def statistics(): AnyRef = store.statistics match {
    case Some(stats) => stats.asInstanceOf[AnyRef]
    case None => ActionResult(404, "Statistics requested but none gathered!")
  }

  @Action(method="POST", path="/transactions")
  def transactions(requestData: RequestData): ActionResult[_] = store.store(requestData) match {
    case Left(errMessage) => ActionResult(204, errMessage)
    case Right(_) => ActionResult(201, "Successfully added!")
  }

}